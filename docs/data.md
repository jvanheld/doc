---
title: Manage your data (get/put/edit/view)
---

# Manage your data

## Storage

Several volumes of data storage are available on the NNCR cluster. Use it knowingly.

|                      | Usage                                   | Quota (default)  | Backup policy |
| -------------------- | --------------------------------------- | ----------------:| -------------:|
| **/shared/home**     | Home directory (personnal data)         |            200GB |            NA |
| **/shared/projects** | Scientific and project (common data)    |              1TB |            NA |
| **/shared/bank** | Read-Only. ommon banks (*UniProt*, *RefSeq*, ...)   |          |               |

If you need or plan to use more storage space, please [contact-us](mailto:contact-nncr-cluster@groupes.france-bioinformatique.fr).

<!--
**Restoring data (snapshots):**  
If you change or remove data by error, you can try to get back your data thanks to snapshots.
Simply browse the .snpashots directory (/shared/home/.snapshots/ or /shared/projects/.snapshots/), sorted by date, and visualize/copy/restore your data. 
-->


## Transfer

SSH protocol (or SFTP - SSH File Transfer Protocol) is the only protocol available. 
But, you can also use clients on the cluster to download your data (*wget*, *curl*, *scp*, *rsync*, *wget*, *ftp*, etc.).

You can get/put your data as described below.

### Command line

On your workstation or on the cluster, you can use command line with your favorite client: scp, rsync, etc.
<asciinema-player src="../casts/transfer.cast"  id="asciicast-transfer" async poster="npt:0:11" speed="1.5" preload="true" data-autoplay="false"></asciinema-player>

### SFTP Graphic client

Use a graphic client like [FileZilla](https://filezilla-project.org/) on your workstation.  
<video width="572" controls><source src="../casts/filezilla_lowquality.mp4" type="video/mp4">Your browser does not support the video tag.</video>

Or use your file manager directly if it's possible (SFTP support).
<video width="572" controls><source src="../casts/sftp_ubuntu-18-04.mp4" type="video/mp4">Your browser does not support the video tag.</video>

### SSHFS

Use your file manager with [SSHFS](https://github.com/libfuse/sshfs)  
<video width="572" controls><source src="../casts/sshfs.mp4" type="video/mp4">Your browser does not support the video tag.</video>



## Editors

Several editors and tools are available on the cluster:

* vim
* emacs
* nano

Or graphical editors. Graphic tools need some configuration: see [Export display](data.md#export-display).

* xemacs
* geany
* gedit

Or use web interface:

* [RStudio](https://rstudio.cluster.france-bioinformatique.fr/)



### Export display

* Linux / Mac  
  Simply use `-Y` option:  
  ```
  ssh -Y <username>@core.cluster.france-bioinformatique.fr
  ```

* Windows  
  1\. Install a X server (for example: [VcXsrv Windows X Server](https://sourceforge.net/projects/vcxsrv/))  
  2\. Launch the X server  
  3\. Activate X11 forwarding  
  <a onclick="toggle_visibility('putty_x11forwarding');">> Example with PuTTY</a>  
  <img id="putty_x11forwarding" class="hide" src="../imgs/putty_x11forwarding_highlighting.png" alt="X11 Forwarding with PuTTY"></img>  

Once connected, you can launch your program with a graphical user interface.





## Manage

Several possibilities to browse, manage and edit or view your data/scripts

* Directly on the cluster with the command line

* Get the data back on your workstation (see [Transfer](quick-start.md#transfer)) and work locally.

* Use SSHFS on your workstation (see above: [SSHFS](data.md#sshfs))

