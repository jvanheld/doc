---
title: IFB Core Cluster documentation
---

# Welcome to the IFB Core Cluster documentation

The IFB Core Cluster is part of the National Network of Compute Resources (NNCR) of the Institut Français de Bioinformatique (IFB).

![IFB logo](./imgs/logo-ifb_RVB.jpg)  



| Current available services |  |
|--:|---|
| ssh://**core**.cluster.france-bioinformatique.fr  | High Performance Computing  |
| [https://**rstudio**.cluster.france-bioinformatique.fr](https://rstudio.cluster.france-bioinformatique.fr)  | R in your browser |
|  [https://**community**.cluster.france-bioinformatique.fr](https://community.cluster.france-bioinformatique.fr) | Mutual help and community support |
| [https://**gitlab**.com/ifb-elixirfr](https://gitlab.com/ifb-elixirfr) | Contribute ! |
