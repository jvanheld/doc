# Singularity (Beta)

## About Singularity
Singularity is a free, cross-platform and open-source computer program that performs operating-system-level virtualization also known as containerization.

One of the main uses of Singularity is to bring containers and reproducibility to scientific computing and the high-performance computing (HPC) world.

The need for reproducibility requires the ability to use containers to move applications from system to system.

Using Singularity containers, developers can work in reproducible environments of their choosing and design, and these complete environments can easily be copied and executed on other platforms.

([source](https://en.wikipedia.org/wiki/Singularity_(software))


## How using Singularity

```
$ singularity run shub://GodloveD/lolcow
Progress |===================================| 100.0%
________________________________________
/ RTFM:                                 \
\ Read The Fucking Manual               /
----------------------------------------
       \   ^__^
        \  (oo)\_______
           (__)\       )\/\
               ||----w |
               ||     ||
```

The documentation is here: [User Guide](https://www.sylabs.io/guides/2.6/user-guide/index.html)

A selection for survival:
 - [Executing Commands](https://www.sylabs.io/guides/2.6/user-guide/quick_start.html#executing-commands)
 - [Working with Files](https://www.sylabs.io/guides/2.6/user-guide/quick_start.html#working-with-files)
 - [Compatibility with standard work-flows, pipes and IO](https://www.sylabs.io/guides/2.6/user-guide/introduction.html?#compatibility-with-standard-work-flows-pipes-and-io)

## Where finding Singularity images

### From the BioContainer CVMFS repository

#### CernVM File System

The [CVMFS or CernVM File System or CernVM-FS](https://cvmfs.readthedocs.io/en/stable/) provides a scalable, reliable and low-maintenance software distribution service.

In our case, it will allow us to access some CernVM-FS (CVMFS) repositories that distribute the data in multiple replicas across the United States and Europe provided by the [Galaxy Project](https://galaxyproject.org/admin/reference-data-repo/).

#### singularity.galaxyproject.org

The [Galaxy Project](https://galaxyproject.org/admin/reference-data-repo/) provides some CernVM-FS (CVMFS) repositories: reference data and singularity images.

We have decided to mount the Singularity repository on the IFB cluster.

The Singularity images came from a long process:
 1. Lambda people write some [Conda](https://conda.io/docs/) recipes in the [Bioconda](https://bioconda.github.io/) GitHub [repository](https://github.com/bioconda/bioconda-recipes)
 2. A robot (Continuous Integration processes) build [Conda packages](https://anaconda.org/bioconda)
 3. An other robot build for each Conda package a Docker container for the project [BioContainer](https://biocontainers.pro/) in container repositories: [Docker Hub](https://hub.docker.com/u/biocontainers/) and [Quay.io](https://quay.io/organization/biocontainers)
 4. An other robot (managed by Björn Grüning) build for each Docker container a Singularity image and store it on the Galaxy Project https://depot.galaxyproject.org/singularity/

#### Usage

Singularity images path is `/cvmfs/singularity.galaxyproject.org`

**Note** that the packages are rather old. The last update was the 9-Jan-2018. But there should be some update and a better regularity soon.

```
singularity shell /cvmfs/singularity.galaxyproject.org/s/a/samtools\:1.6--0
```

- `singularity shell` will open a shell within the container to explore the container or run the job

```
singularity exec -B $PWD:/data /cvmfs/singularity.galaxyproject.org/s/a/samtools\:1.6--0 samtools view -hbS -q 30 /data/bam/siNT_ER_E2_r3_chr21_trim.sam | singularity exec -B $PWD:/data /cvmfs/singularity.galaxyproject.org/s/a/samtools\:1.6--0 samtools sort > bam/siNT_ER_E2_r3_chr21_trim.bam

```

 - `singularity exec` launch command within the container
 - `-B $PWD:/data`: will mount your current directory at /data
 - `/data/bam/siNT_ER_E2_r3_chr21_trim.sam`: all files in your current directory will be located in `/data` within the container
 - `|` and `>` Singularity allow the use of pipe and redirection.
 - `bam/siNT_ER_E2_r3_chr21_trim.bam` in that case, the path is the "regular" one

#### Troubleshooting

##### Can't see the repository `singularity.galaxyproject.org`  in the `/cvmfs` folder.

`ls /cvmfs/singularity.galaxyproject.org` will display its contain and the folder itself

##### Some packages/images are buggy

We hope that some update will solve that

### From Docker

Singularity accept to run Docker containers

```
singularity pull docker://godlovedc/lolcow
./lolcow.simg
____________________________________
/ You're currently going through a   \
| difficult transition period called |
\ "Life."                            /
------------------------------------
       \   ^__^
        \  (oo)\_______
           (__)\       )\/\
               ||----w |
               ||     ||
```


## A Full Example

### script slurm

singularity_bowtie2.slurm
```
#!/bin/bash
#SBATCH -o slurm.%N.%j.out
#SBATCH -e slurm.%N.%j.err
#SBATCH --partition fast
#SBATCH --cpus-per-task 2
#SBATCH --mem 10GB


mkdir fastq
wget http://denis.puthier.perso.luminy.univ-amu.fr/siNT_ER_E2_r3_chr21.fastq.gz
gunzip siNT_ER_E2_r3_chr21.fastq.gz
mv siNT_ER_E2_r3_chr21.fastq fastq

mkdir trimmed
singularity exec -B $PWD:/data /cvmfs/singularity.galaxyproject.org/s/i/sickle-trim\:1.33--1 sickle se -f /data/fastq/siNT_ER_E2_r3_chr21.fastq -t sanger -o /data/trimmed/siNT_ER_E2_r3_chr21_trim.fastq

mkdir index
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/chromosomes/chr21.fa.gz
gunzip chr21.fa.gz
mv chr21.fa index
singularity exec -B $PWD:/data /cvmfs/singularity.galaxyproject.org/b/o/bowtie2:2.3.3.1--py36pl5.22.0_0 bowtie2-build /data/index/chr21.fa /data/index/chr21_hg19

mkdir bam
singularity exec -B $PWD:/data /cvmfs/singularity.galaxyproject.org/b/o/bowtie2:2.3.3.1--py36pl5.22.0_0 bowtie2 --threads $SLURM_CPUS_PER_TASK -x /data/index/chr21_hg19 -U /data/trimmed/siNT_ER_E2_r3_chr21_trim.fastq -S /data/bam/siNT_ER_E2_r3_chr21_trim.sam
singularity exec -B $PWD:/data /cvmfs/singularity.galaxyproject.org/s/a/samtools\:1.6--0 samtools view -hbS -q 30 /data/bam/siNT_ER_E2_r3_chr21_trim.sam | singularity exec -B $PWD:/data /cvmfs/singularity.galaxyproject.org/s/a/samtools\:1.6--0 samtools sort > bam/siNT_ER_E2_r3_chr21_trim.bam
singularity exec -B $PWD:/data /cvmfs/singularity.galaxyproject.org/s/a/samtools\:1.6--0 samtools index /data/bam/siNT_ER_E2_r3_chr21_trim.bam

singularity exec -B $PWD:/data /cvmfs/singularity.galaxyproject.org/s/a/samtools\:1.6--0 samtools view -h /data/bam/siNT_ER_E2_r3_chr21_trim.bam | head -20
```

### sbatch

`sbatch singularity_bowtie2.slurm`

### Result

`cat slurm.*.out`

```
@HD  VN:1.0  SO:coordinate
@SQ     SN:chr21        LN:48129895
@PG     ID:bowtie2      PN:bowtie2      VN:2.3.3.1      CL:"/usr/local/bin/bowtie2-align-s --wrapper basic-0 -x /data/index/chr21_hg19 -S /data/bam/siNT_ER_E2_r3_chr21_trim.sam -U /data/trimmed/siNT_ER_E2_r3_chr21_trim.fastq"
SRR540192.3916166       16      chr21   9411377 42      37M     *       0       0       CTCTGTTCCTTGCTGACCTCCAATTTTGTCTATCCTT   GHFFFEGBHHHHHBHHHHHHHHFFDEHHHHHHHHHHH   AS:i:0  XN:i:0  XM:i:0  XO:i:0  XG:i:0  NM:i:0  MD:Z:37 YT:Z:UU
SRR540192.7673103       0       chr21   9411551 42      37M     *       0       0       CCGGCCCTTTATCAAGCCCTGGCCACCATGATAGTCA   HHHHFHHHHHHHHHHDHHHHGGGGFHGDHHGGGGEGG   AS:i:0  XN:i:0  XM:i:0  XO:i:0  XG:i:0  NM:i:0  MD:Z:37 YT:Z:UU
SRR540192.25047223      16      chr21   9411593 42      37M     *       0       0       TCCAATTGTTGTCTATGCAGGCCTACCAGATTTCTAA   IIEIIIIDIIIIIIIIIIIIIIIIIIIIIIIIIIIII   AS:i:0  XN:i:0  XM:i:0  XO:i:0  XG:i:0  NM:i:0  MD:Z:37 YT:Z:UU
SRR540192.11503770      0       chr21   9411670 42      37M     *       0       0       CAAATGTATCCAAATGAAAGGCTGTGGAGAATGTTGA   IIIIIIIIIIIIIIIIIIIIIIIIGIIIIHIFHIHII   AS:i:0  XN:i:0  XM:i:0  XO:i:0  XG:i:0  NM:i:0  MD:Z:37 YT:Z:UU
SRR540192.21957275      0       chr21   9411756 42      37M     *       0       0       TCTGGATGCTTTGATTGCTATCAGAAGCCGTTAAATA   IIIIIIIIIIIIIIIIIIIHIIIHHIIIIIIIIIIHG   AS:i:0  XN:i:0  XM:i:0  XO:i:0  XG:i:0  NM:i:0  MD:Z:37 YT:Z:UU
SRR540192.2371243       0       chr21   9412286 42      37M     *       0       0       CATAAAATGAGTTCTAGAGTTTATTTCTTTACTGCAT   GDG<GEAAEA=5;==DDBB?EBED@=@E@@ADBGG><   AS:i:0  XN:i:0  XM:i:0  XO:i:0  XG:i:0  NM:i:0  MD:Z:37 YT:Z:UU
SRR540192.16647265      0       chr21   9412303 42      37M     *       0       0       AGTTTATTTCTTTACTGCATCATTCTATTTTCAAGTC   IIIIIIIIIIIIIIIIIIIIIIIIIIHIIIIIIIIII   AS:i:0  XN:i:0  XM:i:0  XO:i:0  XG:i:0  NM:i:0  MD:Z:37 YT:Z:UU
SRR540192.8808648       16      chr21   9412397 42      37M     *       0       0       TTCATATTTTATTTTTTATTTACTGTATAATTCAGTA   IIIHHIGIIIGIIIIIGHIHHIIIIIIIIIIIIIIII   AS:i:0  XN:i:0  XM:i:0  XO:i:0  XG:i:0  NM:i:0  MD:Z:37 YT:Z:UU
SRR540192.24964502      0       chr21   9412572 42      37M     *       0       0       CAAATTTGGAAAACTGGAAAAAATATACATGGCAACA   IIIIIIIIIIIIIIIIIGIIIIIIIIIIIIIIIIIII   AS:i:0  XN:i:0  XM:i:0  XO:i:0  XG:i:0  NM:i:0  MD:Z:37 YT:Z:UU
SRR540192.14173679      0       chr21   9412633 42      37M     *       0       0       CAAACAACTATAAATATTGTTCCACCCAAACAACTAT   IIIIIIIIIIIIIIHIIIHIII3IIIIIIIIIIIIHH   AS:i:-3 XN:i:0  XM:i:1  XO:i:0  XG:i:0  NM:i:1  MD:Z:22T14      YT:Z:UU
SRR540192.28855342      0       chr21   9412728 42      37M     *       0       0       ATTTACAATGGTGTAAACTGTTATACACCATTTATTT   IIIHIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII   AS:i:0  XN:i:0  XM:i:0  XO:i:0  XG:i:0  NM:i:0  MD:Z:37 YT:Z:UU
```
